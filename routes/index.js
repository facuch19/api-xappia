const express = require('express');
const api = express.Router();

// CONTROLLERS
const CompanyController = require('../controllers/CompanyController');

// ENDPOINTS
api.get('/', CompanyController.getAll);
api.get('/:id', CompanyController.getById);

module.exports = api;
