const { Model } = require('objection');
const path = require('path');

class Company extends Model {
  static get tableName () {
    return 'companies';
  }

  $beforeInsert() {
    this.created_at = new Date().toISOString();
  }

  $beforeUpdate() {
    this.updated_at = new Date().toISOString();
  }
}

module.exports = Company;
