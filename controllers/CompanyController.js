const Company = require('../models/Company');

module.exports = {
	getAll: async (req, res) => {
		try {
			let companies = await Company.query();

			if (companies.length === 0) return res.status(404).send({ code: 404, message: 'There arent companies' });

			res.status(200).send({
				code: 200,
				message: 'Found Companies',
				data: companies
			});

		} catch (err) {
			res.status(500).send({ code:505, message: `${err}` });
		};
	},
	getById: async (req, res) => {
		
		let companyId = req.params.id;
		
		try {
			let company = await Company.query().where('id', companyId);
		
			if (company.length === 0) return res.status(404).send({ code: 404, message: 'Company Not Found.' });

			res.status(200).send({
				code: 200,
				message: 'Found Company!',
				data: company
			});
		} catch (err) {
			res.status(500).send({ code:505, message: `${err}` });
		};
	},
}
