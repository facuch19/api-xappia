# Detalles para la DB

  - Configurar el string de conexion en db/knexfile.js
  - Crear una base de datos llamada xappia (o modificar el string de conexion en: db/knexfile.js)
  - Correr las migraciones con: npm run db:migrate.
  - Correr los seeders con: npm run db:seed.
  - Ya se pueden consumir los datos.
