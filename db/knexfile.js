module.exports = {

  development: {
    client: 'mysql',
    connection: process.env.DATABASE_URL || {
      host : '127.0.0.1',
      user : 'root',
      password : '1234',
      database : 'xappia'
    },
    useNullAsDefault: true
  },

  production: {
    // production data...
  }

}
