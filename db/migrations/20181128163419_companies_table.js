exports.up = (knex, Promise) => (
    Promise.all([
      knex.schema.createTable('companies', table => {
        table.string('id', 50).unique().notNullable(),
        table.boolean('isActive'),
        table.string('balance', 450),
        table.integer('age'),
        table.string('firstName', 50),
        table.string('lastName', 50),
        table.string('company', 50),
        table.string('email', 50),
        table.string('phone', 50),
        table.string('address', 50),
        table.string('about', 500),
        table.string('registered', 50),
        table.string('latitude', 50),
        table.string('longitude', 50)
      })
    ])
);
  
exports.down = (knex, Promise) => (
    Promise.all([
      knex.schema.dropTable('companies')
    ])
);