const config = require('./config');
const app = require('./app');

require('./db/setup');

app.listen(config.PORT, () => {
  console.log(`Server listening in port: ${config.PORT}`);
});
